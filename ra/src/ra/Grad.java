package ra;

public class Grad {
	private int broj;
	private String ime;
	
	public Grad(int broj, String ime) {
		super();
		this.broj = broj;
		this.ime = ime;
	}
	public int getBroj() {
		return broj;
	}
	public void setBroj(int broj) {
		this.broj = broj;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}

}
