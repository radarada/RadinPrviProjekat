package ra;

public class Manifestacija {
	
	private String id;
	private String naziv;
	private int brojPosetioca;
	private String gradManifestacije;
	
	public Manifestacija(String id, String naziv, int brojPosetioca, String gradManifestacije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetioca = brojPosetioca;
		this.gradManifestacije = gradManifestacije;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojPosetioca() {
		return brojPosetioca;
	}

	public void setBrojPosetioca(int brojPosetioca) {
		this.brojPosetioca = brojPosetioca;
	}

	public String getGradManifestacije() {
		return gradManifestacije;
	}

	public void setGradManifestacije(String gradManifestacije) {
		this.gradManifestacije = gradManifestacije;
	}

	
	
	
}
